﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public interface Product
    {
        //价格
        int price { get; set; }
        //名字
        String name { get; set; }
        void make();
    }
}
