﻿using System;

namespace DecoratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Additives drink1 = new Pudding(new Pearl(new MilkGreenTea()));
            Additives drink2 = new Pudding(new Pearl(new MilkRedTea()));
            Additives drink3 = new Pearl(new Pearl(new MilkRedTea()));
            drink1.make();
            drink2.make();
            drink3.make();
        }
    }
}
