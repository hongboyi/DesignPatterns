﻿using System;

namespace ObserverPattern
{
    class ObserverPatternDemo
    {
        static void Main(string[] args)
        {
            Material material = new Material();
            new Product1(material);
            new Product2(material);
            Console.WriteLine("原材料价格改为15");
            material.setPrice(15);
            Console.WriteLine("原材料价格改为18");
            material.setPrice(18);

        }
    }
}
