﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern
{
    class Product1 : Observer
    {
        public Product1(Material material){
            this.material = material;
            this.material.attach(this);
        }
        public override void update()
        {
            //计算商品价格
            int price = 13 + this.material.getPrice();
            Console.WriteLine("商品1价格更新为："+price);
        }
    }
}
