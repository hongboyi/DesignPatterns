﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Singleton
{
    class TestSingleton
    {
        //设置名字
        String name = null;  
        //创建实例并初始化
        private static TestSingleton instance = new TestSingleton();

        //构造函数
        private TestSingleton()
        {
        }

        //返回实例
        public static TestSingleton getInstance()
        {
            return instance;
        }

        public String getName() {  
                return name;  
        }  
  
        public void setName(String name) {  
            this.name = name;  
        }  
  
        //输出name
        public void printInfo() {  
            Console.WriteLine("the name is " + name);  
        }  
    }
}
