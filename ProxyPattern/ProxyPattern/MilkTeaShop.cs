﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern
{
    class MilkTeaShop:Shop
    {
        public virtual void BuyDrinks()
        {
            Console.WriteLine("奶茶店收取费用");
        }
    }
}
