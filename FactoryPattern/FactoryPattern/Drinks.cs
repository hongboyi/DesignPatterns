﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern
{
    public interface Drinks
    {
        //定义饮料类型
        string name { get; set; }
        //定义方法
        void make();
    }
}
