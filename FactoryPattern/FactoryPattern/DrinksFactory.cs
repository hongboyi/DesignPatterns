﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern
{
    class DrinksFactory
    {
        public Drinks getDrinks(String drinkType)
        {
            if (drinkType == null) 
            {
                return null;
            }
            if (drinkType.Equals("COFFEE"))
            {
                return new Coffee();
            }else if (drinkType.Equals("MILKTEA"))
            {
                return new MilkTea();
            }
            return null;
        }
    }
}
